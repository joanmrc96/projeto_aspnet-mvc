﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjetoASPNET_MVC.Classes;

namespace ProjetoASPNET_MVC.Classes
{
    public class Endereco
    {
        public string Rua { get; set; }
        public int Numero { get; set; }

    }
}