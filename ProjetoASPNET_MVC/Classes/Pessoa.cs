﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjetoASPNET_MVC.Classes
{
    public class Pessoa
    {
        public string Nome { get; set; }
        public DateTime Nascimento { get; set; }
        public int Cpf { get; set; }
        public Endereco Endereco {get; set;}

        
    }
}