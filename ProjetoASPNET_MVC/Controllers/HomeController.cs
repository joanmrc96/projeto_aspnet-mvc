﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjetoASPNET_MVC.Classes;

namespace ProjetoASPNET_MVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Pessoa pessoa = new Pessoa();
            pessoa.Endereco = new Endereco();

            pessoa.Nome = "Joan Marcos";
            pessoa.Nascimento = DateTime.Now;
            pessoa.Cpf = 0774363250;
            pessoa.Endereco.Rua = "Portao";
            pessoa.Endereco.Numero = 15;

            Pessoa PessoaMaster = new Pessoa();
            PessoaMaster.Endereco = new Endereco();

            PessoaMaster.Nome = "Joan Marcos";
            PessoaMaster.Nascimento = DateTime.Now;
            PessoaMaster.Cpf = 0774363250;
            PessoaMaster.Endereco.Rua = "Portao";
            PessoaMaster.Endereco.Numero = 15;

            List<Pessoa> listaPessoa = new List<Pessoa>();

            listaPessoa.Add(pessoa);
            listaPessoa.Add(PessoaMaster);

            return View(listaPessoa);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public ActionResult EditPost(Pessoa pessoa)
        {
            try {
                return View();
            }
            catch (Exception ex)
            {
                return View();
            }
        }
        
        public ActionResult Edit()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }
    }
}